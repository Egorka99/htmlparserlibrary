package com.epam;

import org.junit.Before;
import org.junit.Test;
import java.io.IOException;
import static org.junit.Assert.*;

/**
 * Unit test for ParserLibrary
 */
public class ParserTests
{
    HtmlParser parser;

    String regexpSearchHtmlEntities;

    @Before
    public void setUp() throws IOException {
        parser = new HtmlParser();

        regexpSearchHtmlEntities = "((.|\n)*<br>(.|\n)*)|((.|\n)*&lt;(.|\n)*)|((.|\n)*&gt;(.|\n)*)" +
                "|((.|\n)*&amp;(.|\n)*)|((.|\n)*&quot;(.|\n)*)";
    }

    @Test
    public void shouldGetProcessedQuote() throws InputDataException {

        String notProcessedHtmlCode = "<div class=\"quote__body\">\n" +
                "      &lt;}{Lighter&gt; знаете что такое линукс?<br>&lt;}{Lighter&gt; мне тут юзверь выдал<br>&lt;}" +
                "{Lighter&gt; \"ну это же типо виндовз, но программы надо выполнять, набором букв, а не мышкой щёлкать\"\n" +
                "          </div>";


        assertEquals( "<}{Lighter> знаете что такое линукс?\n<}{Lighter> мне тут юзверь выдал" +
                "\n<}{Lighter> " +
                "\"ну это же типо виндовз," +
                " но программы надо выполнять, набором букв, а не мышкой щёлкать\"", parser.getQuote(notProcessedHtmlCode));
    }


    @Test
    public void shouldGetQuoteWithoutHtmlEntities() throws InputDataException {

        String notProcessedHtmlCode = "<div class=\"quote__body\">\n" +
                "                &lt;Ares_&gt; мда.. на работе клиента не убил.. ща ;)<br>&lt;Azareth&gt; заповедь: \"" +
                "Уходишь с работы - УБЕЙ КЛИЕНТА :ЕЕЕ\"\n" +
                "                </div>";

        assertEquals( "<Ares_> мда.. на работе клиента не убил.. ща ;)\n" +
                "<Azareth> заповедь: \"Уходишь с работы - УБЕЙ КЛИЕНТА :ЕЕЕ\"", parser.getQuote(notProcessedHtmlCode));

    }

    @Test(expected = InputDataException.class)
    public void inputIncorrectDataShouldGetInputDataException() throws InputDataException {

        String incorrectData = "Some input incorrect data";
        parser.getQuote(incorrectData);
    }

    @Test(expected = InputDataException.class)
    public void inputDataWithoutOpenTagWithQuoteShouldGetInputDataException() throws InputDataException {

        String notProcessedHtmlCode =
                "                &lt;Ares_&gt; мда.. на работе клиента не убил.. ща ;)<br>&lt;Azareth&gt; заповедь: \"" +
                "Уходишь с работы - УБЕЙ КЛИЕНТА :ЕЕЕ\"\n" +
                "                </div>";

        parser.getQuote(notProcessedHtmlCode);
    }

    @Test(expected = InputDataException.class)
    public void inputDataWithoutCloseTagWithQuoteShouldGetInputDataException() throws InputDataException {

        String notProcessedHtmlCode = "<div class=\"quote__body\">\n" +
                "                &lt;Ares_&gt; мда.. на работе клиента не убил.. ща ;)<br>&lt;Azareth&gt; заповедь: \"" +
                "Уходишь с работы - УБЕЙ КЛИЕНТА :ЕЕЕ\"\n" +
                "                ";

        parser.getQuote(notProcessedHtmlCode);
    }


}

package com.epam;


import org.junit.Test;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class RequestTests {

    @Test
    public void shouldGetHTMLCodeFromTheSite() throws IOException, InputDataException {
        HtmlResponse response = new HtmlResponse("123");
        assertFalse(response.getHtmlResponse().isEmpty());
    }

    @Test(expected = InputDataException.class)
    public void inputNullShouldReturnIOException() throws IOException, InputDataException {
        HtmlResponse response = new HtmlResponse(null);
        response.getHtmlResponse();
    }

    @Test(expected = InputDataException.class)
    public void inputNotExistentLinkShouldReturnIOException() throws IOException, InputDataException {
        HtmlResponse response = new HtmlResponse("@/52");
        response.getHtmlResponse();
    }

    @Test
    public void inputLongNumberShouldReturnQuoteNotFound() throws IOException, InputDataException {

        HtmlResponse response = new HtmlResponse("3243253535352");

        assertEquals("Такой цитаты не найдено!", response.getHtmlResponse());
    }

}

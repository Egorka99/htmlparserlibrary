package com.epam;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * This class get html code form the site https://bash.im/ (page with quote);
 */
public class HtmlResponse {

    private static String url;
    private static String stopWords;

    private String numberOfQuote;

    /**
     * Constructor
     * @param numberOfQuote the quote number
     */
    public HtmlResponse(String numberOfQuote) throws IOException {
        this.numberOfQuote = numberOfQuote;
        Config config = Config.getInstance();
        stopWords = "<title>Цитатник Рунета</title>"; //.properties not read UTF-8
        url = config.getUrl();
    }

    /**
     * Getting HTML response to GET request to the site
     * @return HTML response
     */
    public String getHtmlResponse() throws IOException, InputDataException {

        StringBuilder responseFormer = new StringBuilder();
        if (!tryParseLong(numberOfQuote)) throw new InputDataException("Неверный номер цитаты");
        URL obj = new URL(url + numberOfQuote);
        HttpURLConnection connection = (HttpURLConnection) obj.openConnection();
        connection.setRequestMethod("GET");

        BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String inputLine;

        while ((inputLine = in.readLine()) != null) {
            responseFormer.append(inputLine);
        }
        in.close();

        String response = responseFormer.toString();

        if (response.contains(stopWords)) {
            response = "Такой цитаты не найдено!";
        }

        return response;
    }

    private static boolean tryParseLong(String value) {
        try {
            Long.parseLong(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

}

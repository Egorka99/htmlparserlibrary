package com.epam;


import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Config {

    private static Config instance;

    private String configFilePath = "application.properties";
    private String url;
    private String quoteHtmlTag;

    private Config() throws IOException {

        InputStream is = getClass().getClassLoader().getResourceAsStream(configFilePath);
        Properties property = new Properties();
        property.load(is);

        url = property.getProperty("url");
        quoteHtmlTag = property.getProperty("quoteHtmlTag");

    }

    public static synchronized Config getInstance() throws IOException {
        if (instance == null) {
            instance = new Config();
        }
        return instance;
    }

    public String getUrl() {
        return url;
    }

    public String getQuoteHtmlTag() {
        return quoteHtmlTag;
    }
}


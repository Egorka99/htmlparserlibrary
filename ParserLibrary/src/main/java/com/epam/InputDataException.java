package com.epam;

public class InputDataException extends Exception {
    public InputDataException(String message) {
        super(message);
    }
}

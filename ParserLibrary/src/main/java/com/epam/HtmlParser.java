package com.epam;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * This class work with https://bash.im/. It has method for parsing HTML and getting correct quote.
 * @author Egor_Zavgorodnev
 */
public class HtmlParser {

    private static String quoteHtmlTag;
    private static Map<String, String> htmlEntities;

    /**
     * Constructor
     */
    public HtmlParser() throws IOException {

        Config config = Config.getInstance();
        quoteHtmlTag = config.getQuoteHtmlTag();
        htmlEntities = new HashMap<>();
    }

    /**
     * Getting quote (String) by quote number
     * @return quote from the site
     */
    public String getQuote(String response) throws InputDataException {
        htmlEntitiesInit();

        int openTagIndex = response.indexOf(quoteHtmlTag);
        if (openTagIndex == -1) {
            throw new InputDataException("Не найден открывающийся тэг с цитатой");
        }
        int closeTagIndex = response.substring(openTagIndex).indexOf("</div>");

        if (closeTagIndex == -1) {
            throw new InputDataException("Не найден закрывающийся тэг с цитатой");
        }

        String clearText = response.substring(openTagIndex + quoteHtmlTag.length(), openTagIndex + closeTagIndex);
        // delete <br> tags and html entities like &lt; &gt;
        for (Map.Entry<String, String> item: htmlEntities.entrySet()) {
            clearText = clearText.replaceAll(item.getKey(),item.getValue());
        }

        return clearText.trim();
    }

    /**
     * Initialization HTML entities for correct processing
     */
    private static void htmlEntitiesInit() {
        if (htmlEntities.size() == 0) {
            htmlEntities.put("<br>","\n");
            htmlEntities.put("<br />","\n");
            htmlEntities.put("&lt;","<");
            htmlEntities.put("&gt;",">");
            htmlEntities.put("&amp;","&");
            htmlEntities.put("&quot;","\"");
        }
    }


}
